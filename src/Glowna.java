import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Glowna
{
    public static void main(String[] args)
    {
        int a, b, n;
        Scanner skaner = new Scanner(System.in);

        System.out.println("Podaj czesc rzeczywista: ");
        a = skaner.nextInt();
        System.out.println("Podaj czesc urojoną: ");
        b = skaner.nextInt();

        Zespolona liczba1 = new Zespolona(a, b);

        System.out.println("Podaj wielkosc tablicy: ");
        n = skaner.nextInt();
        List<Zespolona> lista_zespolona = new ArrayList<Zespolona>(n);
        for (int i = 0; i < n; i++)
        {
            lista_zespolona.add(new Zespolona());
        }

        liczba1.wyswietlanie();
        System.out.println("+");
        lista_zespolona.get(2).wyswietlanie();
        System.out.println("=");
        liczba1.dodawanie(lista_zespolona.get(2));
        liczba1.wyswietlanie();
    }
}