import java.util.Random;

public class Zespolona
{
    double a;
    double b;

    //Kostruktory
    public Zespolona()
    {
        Random randomizer = new Random();
        a = randomizer.nextDouble();
        b = randomizer.nextDouble();
    }

    public Zespolona(int _a, int _b)
    {
        a = _a;
        b = _b;
    }

    //Reszta
    public double modul()
    {
        double pot_a = Math.pow(a, 2);
        double pot_b = Math.pow(b, 2);
        double modul = Math.sqrt(pot_a + pot_b);
        return modul;
    }

    public Zespolona sprzezenie()
    {
        Zespolona nowa = new Zespolona();
        nowa.a = a;
        nowa.b = -b;
        return nowa;
    }

    public void dodawanie(Zespolona z)
    {
        a += z.a;
        b += z.b;
    }

    public void odejmowanie(Zespolona z)
    {
        a -= z.a;
        b -= z.b;
    }

    public void mnozenie(Zespolona z)
    {
        a = (a * z.a) - (b * z.b);
        b = (a * z.b) + (b * z.a);
    }

    public void wyswietlanie()
    {
        if (b >= 0)
            System.out.println(a + "+" + b);
        else
            System.out.println(a + "" + b);
    }
}
